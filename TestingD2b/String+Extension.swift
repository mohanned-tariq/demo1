//
//  Config.swift
//  D2p
//
//  Created by Mohanned on 1/28/18.
//  Copyright © 2018 Mohanned. All rights reserved.
//
import Foundation


extension String {
    
    var trimmed: String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
}
