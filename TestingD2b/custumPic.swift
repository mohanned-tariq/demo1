//
//  custum.swift
//  D2p
//
//  Created by Mohanned on 1/24/18.
//  Copyright © 2018 Mohanned. All rights reserved.
//

import UIKit

class CheckboxButton: UIButton {
    
  
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setup()
    }
    
    // Initialize from Storyboard
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setup()
    }
    
    func setup() {   // be3mel custom
        
        self.backgroundColor = UIColor.red
        self.setTitleColor(UIColor.white, for: UIControlState.normal)
        
        self.layer.cornerRadius = self.frame.size.height / 2
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor.white.cgColor
        
        
    }
    
}

