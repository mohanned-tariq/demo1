//
//  Config.swift
//  D2p
//
//  Created by Mohanned on 1/29/18.
//  Copyright © 2018 Mohanned. All rights reserved.
//


import UIKit
import SwiftyJSON



//"id": 32,
//"name": "moha",
//"email": "t@gmail.com",
//"level": "patient",
//"mobile": "0",
//"group_id": null,
//"address": null,
//"photo": null,

class Task: NSObject, NSCopying {
    
    var id: Int
    var name: String
    var photo:String
    

    
    
    init(id: Int = 0) {
        self.id = id
        self.name = name
        self.photo = photo
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        let copiedname = name(id: self.id, name: self.name,self.photo = photo)
    
        
        
        return copiedname
    }
    
    init?(dict: [String: JSON]) {
        guard let id = dict["id"]?.toInt, let name = dict["name"]?.string , let photo = dict["photo"]?.toImagePath else { return nil }
        
        self.id = id
        self.name = name
        self.photo = photo
        
        self.completed  =  dict["completed"]?.toBool ?? false
        
        self.created_at = dict["created_at"]?.string ?? ""
    }
}
