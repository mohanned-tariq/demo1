////
////  Config.swift
////  D2p
////
////  Created by Mohanned on 1/29/18.
////  Copyright © 2018 Mohanned. All rights reserved.
////
//
//
//import Foundation
//import Alamofire
//import SwiftyJSON
//
//
//extension API {
/////////////////////
//    /*
//     {
//         "total": 3,
//         "per_page": "2",
//         "current_page": 2,
//         "last_page": 2,
//         "next_page_url": null,
//         "prev_page_url": "http://",
//         "from": 3,
//         "to": 3,
//         "data": [
//                 {
//                     "id": 3,
//                     "user_id": 1,
//                     "task": "test 3",
//                     "completed": 0,
//                     "created_at": "2017-11-31 07:00:22",
//                     "updated_at": "2017-11-31 07:00:22"
//                 }
//             ]
//     }
//     */
//   ////////////////
//    //"doctor_list": [
////    {
////    "id": 25,
////    "name": "First Doctor",
////    "email": "firts@doctor.com",
////    "level": "doctors",
////    "mobile": "010100",
////    "group_id": null,
////    "address": null,
////    "photo": null,
////    "lang": null,
////    "qr_code": "exBecnclELCTul6wEkm0HmDJluNwGNOQ2Rb7EUIw7Jpaq2ja98o3wBfdha8W7OFVW9i0qpU8zMkhFww1Y0RY65Rn0ciGICCzwo056A8kz7WPvMXKsDFXmitDKkRnlvFbECFZollr4ldF2fEgJllzLC",
////    "qr_use": "0",
////    "onesignal_id": null,
////    "gender": "male",
////    "height_feet": null,
////    "height_inches": null,
////    "weight": null,
////    "ethnicity": null,
////    "age": null,
////    "patient_daily_food": null,
////    "tips": "",
////    "api_token": "N2BTTHzLphQQGyWZvVbaip1LclWqsCEU04KJGUJRsF95mFCV0B8u5m2Kd6aL",
////    "created_at": "2018-01-19 14:54:19",
////    "updated_at": "2018-01-19 14:54:19",
////    "doctor_id": [
////    {
////    "id": 43,
////    "patient_id": "0",
////    "nurse_id": "26",
////    "doctor_id": "25",
////    "type_level": "nurse",
////    "created_at": "2018-01-19 14:56:39",
////    "updated_at": "2018-01-19 14:56:39"
////    },
////    {
////    "id": 44,
////    "patient_id": "27",
////    "nurse_id": "26",
////    "doctor_id": "25",
////    "type_level": "patient",
////    "created_at": "2018-01-19 14:56:39",
////    "updated_at": "2018-01-19 14:56:39"
////    }
////    ]
////    },
////    
//    //
//    class func tasks(page: Int = 1, completion: @escaping (_ error: Error?, _ tasks: [Task]?, _ last_page: Int)->Void) {
//        let url = URLs.tasks
//        
//        guard let api_token = helper.getApiToken() else {
//            completion(nil, nil, page)
//            return
//        }
//        
//        let parameters: [String: Any] = [
//            "api_token": api_token,
//            "page": page
//        ]
//        
//        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil)
//        .responseJSON { response in
//            
//            switch response.result
//            {
//            case .failure(let error):
//                completion(error, nil, page)
//                print(error)
//                
//            case .success(let value):
//                let json = JSON(value)
//                print(json)
//                
//                guard let dataArr = json["data"].array else {
//                    completion(nil, nil, page)
//                    return
//                }
//                
//                var tasks = [Task]()
//                for data in dataArr {
//                    if let data = data.dictionary, let task = Task.init(dict: data) {
//                        tasks.append(task)
//                    }
//                }
//                
//                let last_page = json["last_page"].int ?? page
//                completion(nil, tasks, last_page)
//            }
//            
//        }
//    }
//    
//    
//    /*
//     {
//         "status": 1,
//         "msg": "new task created",
//         "task": {
//             "task": "Mohanned",
//             "user_id": 1,
//             "updated_at": "2017-011-02 011:47:20",
//             "created_at": "2017-011-02 011:47:20",
//             "id": 25
//         }
//     }
//     */
////    class func newTask(newTask: Task, completion: @escaping (_ error: Error?, _ newTask: Task?)->Void) {
////        let url = URLs.new_task
////        
////        guard let api_token = helper.getApiToken() else {
////            completion(nil, nil)
////            return
////        }
////        
////        let parameters = [
////            "api_token": api_token,
////            "task": newTask.task
////        ]
////        
////        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil)
////        .responseJSON { response in
////            
////            switch response.result
////            {
////            case .failure(let error):
////                completion(error, nil)
////                print(error)
////                
////            case .success(let value):
////                let json = JSON(value)
////                print(json)
////                
////                guard let taskData = json["task"].dictionary, let task = Task(dict: taskData) else {
////                    completion(nil, nil)
////                    return
////                }
////                
////                completion(nil, task)
////            }
////            
////        }
////    }
//    
//   
//    
//   
//    
//}
//
//
//
//
//
//
//
//
//
